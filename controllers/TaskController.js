// Controllers contain the functions and business logic of our Express JS application

// Allows us to use the contents of the "Task.js" file in the models folder
const Task = require('../models/Task');

// Controller function for getting all the tasks

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	});

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	});
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err) {
			console.log(err);
			return false
		} else {
			return removedTask
		}
	});
}

module.exports.updateTask = (taskId, requestBody) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err);
			return false
		} 

		result.name = requestBody.name;

		return result.save().then((updateTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr)
				return false;
			} else {
				return updateTask
			}
		});
	});
}