// Contains all the endpoints for our application
//  We separate the routes such that "index.js" only contains information on the server

const express = require("express");
const router = express.Router();
const taskController = require("../controllers/TaskController");

// Routes 

// Route to get all the tasks

router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});
// Use "module.exports" to export the router object to use in the index.js"

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;